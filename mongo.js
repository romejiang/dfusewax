import transfers from './transfers.js'
import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost:27017/fwmoniter')

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  let Transfers = transfers()
  let tr = Transfers({
    trx_id: new Date().getTime(),
    timestamp: new Date().getTime(),
    from: 'test',
    to: 'test',
    memo: 'test',
  })
  tr.save((err)=>{
    if (err) {
      console.log(err);
    }
    db.close()
  })
  
})
