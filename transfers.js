import mongoose  from 'mongoose'

export default () => {

  const TransfersSchema = new mongoose.Schema(
    {
      trx_id: { type: String, unique: true, required: true },
      timestamp: { type: Date, required: true },
      from: { type: String, required: true },
      to: { type: String, required: true },
      FWW: { type: Number },
      FWG: { type: Number },
      FWF: { type: Number },
      memo: { type: String, required: true },
    },
    { timestamps: true }
  )

  TransfersSchema.methods.toString = function () {
    return this.name
  }

  return mongoose.model('Transfers', TransfersSchema)
}
